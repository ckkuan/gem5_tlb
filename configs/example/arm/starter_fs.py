# Copyright (c) 2016-2017 ARM Limited
# All rights reserved.
#
# The license below extends only to copyright in the software and shall
# not be construed as granting a license to any other intellectual
# property including but not limited to intellectual property relating
# to a hardware implementation of the functionality of the software
# licensed hereunder.  You may use the software subject to the license
# terms below provided that you ensure that this notice is replicated
# unmodified and in its entirety in all distributions of the software,
# modified or unmodified, in source code or in binary form.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met: redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer;
# redistributions in binary form must reproduce the above copyright
# notice, this list of conditions and the following disclaimer in the
# documentation and/or other materials provided with the distribution;
# neither the name of the copyright holders nor the names of its
# contributors may be used to endorse or promote products derived from
# this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
#  Authors:  Andreas Sandberg
#            Chuan Zhu
#            Gabor Dozsa
#

"""This script is the full system example script from the ARM
Research Starter Kit on System Modeling. More information can be found
at: http://www.arm.com/ResearchEnablement/SystemModeling
"""

from __future__ import print_function

import os
import m5
from m5.util import addToPath
from m5.objects import *
import argparse

m5.util.addToPath('../..')

from common import SysPaths
from common import MemConfig
from common.cores.arm import HPI

import devices


default_dist_version = '20180409'
default_kernel = 'vmlinux.vexpress_gem5_v1_64.' + default_dist_version
default_disk = 'linaro-minimal-aarch64.img'


# Pre-defined CPU configurations. Each tuple must be ordered as : (cpu_class,
# l1_icache_class, l1_dcache_class, walk_cache_class, l2_Cache_class). Any of
# the cache class may be 'None' if the particular cache is not present.
cpu_types = {

    "atomic" : ( AtomicSimpleCPU, None, None, None, None, None),
    "minor" : (MinorCPU,
               devices.L1I, devices.L1D,
               devices.WalkCache,
               devices.L2, None),
    "hpi" : ( HPI.HPI,
              HPI.HPI_ICache, HPI.HPI_DCache,
              HPI.HPI_WalkCache,
              HPI.HPI_L2,
              HPI.HPI_SharedWalkCache)
}

def create_cow_image(name):
    """Helper function to create a Copy-on-Write disk image"""
    image = CowDiskImage()
    image.child.image_file = SysPaths.disk(name)

    return image;

# argparse boolean helper function
def str2bool(v):
        if isinstance(v, bool):
            return v
        if v.lower() in ('yes','true','t','y','1'):
            return True
        elif v.lower() in ('no','false','f','n','0'):
            return False
        else:
            raise argparse.ArgumentTypeError('Boolean value expected.')

def create(args):
    ''' Create and configure the system object. '''

    if not args.dtb:
        dtb_file = SysPaths.binary("armv8_gem5_v1_%icpu.%s.dtb" %
                                   (args.num_cores, default_dist_version))
    else:
        dtb_file = args.dtb

    if args.script and not os.path.isfile(args.script):
        print("Error: Bootscript %s does not exist" % args.script)
        sys.exit(1)

    cpu_class = cpu_types[args.cpu][0]
    mem_mode = cpu_class.memory_mode()
    # Only simulate caches when using a timing CPU (e.g., the HPI model)
    want_caches = True if mem_mode == "timing" else False

    system = devices.SimpleSystem(want_caches,
                                  args.mem_size,
                                  mem_mode=mem_mode,
                                  dtb_filename=dtb_file,
                                  kernel=SysPaths.binary(args.kernel),
                                  readfile=args.script)

    MemConfig.config_mem(args, system)

    # Add the PCI devices we need for this system. The base system
    # doesn't have any PCI devices by default since they are assumed
    # to be added by the configurastion scripts needin them.
    system.pci_devices = [
        # Create a VirtIO block device for the system's boot
        # disk. Attach the disk image using gem5's Copy-on-Write
        # functionality to avoid writing changes to the stored copy of
        # the disk image.
        PciVirtIO(vio=VirtIOBlock(image=create_cow_image(args.disk_image))),
    ]

    # Attach the PCI devices to the system. The helper method in the
    # system assigns a unique PCI bus ID to each of the devices and
    # connects them to the IO bus.
    for dev in system.pci_devices:
        system.attach_pci(dev)

    # Wire up the system's memory system
    system.connect()

    # Add CPU clusters to the system
    system.cpu_cluster = [
        devices.CpuCluster(system,
                           args.num_cores,
                           args.cpu_freq, "1.0V",
                           args.maxinsts,
                           *cpu_types[args.cpu]),
    ]

    # Create a cache hierarchy for the cluster. We are assuming that
    # clusters have core-private L1 caches and an L2 that's shared
    # within the cluster.
    for cluster in system.cpu_cluster:
        system.addCaches(want_caches,
                         last_cache_level=args.last_cache_level,
                         l1i_ret=args.l1i_ret,
                         l1d_ret=args.l1d_ret,
                         l1dw_ret=args.l1dw_ret,
                         l2_ret=args.l2_ret,
                         l3_ret=args.l3_ret,
                         mmul2_ret=args.mmul2_ret,
                         l1d_pfd=args.l1d_pfd,
                         l1i_wrLat=args.l1i_wrLat,
                         l1d_wrLat=args.l1d_wrLat,
                         l1dw_wrLat=args.l1dw_wrLat,
                         mmul2_wrLat=args.mmul2_wrLat,
                         l1d_hitLat=args.l1d_hitLat,
                         l1dw_hitLat=args.l1dw_hitLat,
                         mmul2_hitLat=args.mmul2_hitLat,
                         l1d_pfexp=args.l1d_pfexp,
                         l1d_pfnst=args.l1d_pfnst,
                         mmu_cache=args.mmu_cache
                         )

    # Setup gem5's minimal Linux boot loader.
    system.realview.setupBootLoader(system.membus, system, SysPaths.binary)

    # Linux boot command flags
    kernel_cmd = [
        # Tell Linux to use the simulated serial port as a console
        "console=ttyAMA0",
        # Hard-code timi
        "lpj=19988480",
        # Disable address space randomisation to get a consistent
        # memory layout.
        "norandmaps",
        # Tell Linux where to find the root disk image.
        "root=/dev/vda1",
        # Mount the root disk read-write by default.
        "rw",
        # Tell Linux about the amount of physical memory present.
        "mem=%s" % args.mem_size,
    ]
    system.boot_osflags = " ".join(kernel_cmd)

    return system


def run(args,root):
    cptdir = m5.options.outdir
    if args.checkpoint or args.parsec_checkpoint:
        print("Checkpoint directory: %s" % cptdir)

    while True:
        event = m5.simulate()
        exit_msg = event.getCause()
        if args.parsec_checkpoint and exit_msg == "checkpoint":
            print("Dropping Parsec ROI checkpoint at tick %d" % m5.curTick())
            cpt_dir = os.path.join(m5.options.outdir, "cpt.%d" % m5.curTick())
            m5.checkpoint(os.path.join(cpt_dir))
            print("Checkpoint done.")
            print(exit_msg, " @ ", m5.curTick())
            break
        elif args.checkpoint:
            #pdb breakpoint
            #import pdb; pdb.set_trace()
            cpt_dir = os.path.join(m5.options.outdir, "cpt.%s" % \
            str(root.system.cpu_cluster[0].cpus[0].max_insts_any_thread))
            m5.checkpoint(os.path.join(cpt_dir))
            print("Checkpoint done.")
            print(exit_msg, " @ ", m5.curTick())
            break
        else:
            print(exit_msg, " @ ", m5.curTick())
            break

    sys.exit(event.getCode())


def main():
    parser = argparse.ArgumentParser(epilog=__doc__)

    parser.add_argument("--dtb", type=str, default=None,
                        help="DTB file to load")
    parser.add_argument("--kernel", type=str, default=default_kernel,
                        help="Linux kernel")
    parser.add_argument("--disk-image", type=str,
                        default=default_disk,
                        help="Disk to instantiate")
    parser.add_argument("--script", type=str, default="",
                        help = "Linux bootscript")
    parser.add_argument("--cpu", type=str, choices=cpu_types.keys(),
                        default="atomic",
                        help="CPU model to use")
    parser.add_argument("--cpu-freq", type=str, default="2GHz")
    parser.add_argument("--num-cores", type=int, default=1,
                        help="Number of CPU cores")
    parser.add_argument("--mem-type", default="DDR3_1600_8x8",
                        choices=MemConfig.mem_names(),
                        help = "type of memory to use")
    parser.add_argument("--mem-channels", type=int, default=1,
                        help = "number of memory channels")
    parser.add_argument("--mem-ranks", type=int, default=None,
                        help = "number of memory ranks per channel")
    parser.add_argument("--mem-size", action="store", type=str,
                        default="8GB",
                        help="Specify the physical memory size")
    parser.add_argument("--l1i_ret", action="store", type=str,
                        default="10Tticks",
                        help="Retention time setting of L1I, \
                        unit is 'ticks'(one tick is 10^-12 second)")
    parser.add_argument("--l1d_ret", action="store", type=str,
                        default="10Tticks",
                        help="Retention time setting of L1D, \
                        unit is 'ticks'(one tick is 10^-12 second)")
    parser.add_argument("--l1dw_ret", action="store", type=str,
                        default="10Tticks",
                        help="Retention time setting of L1D walker, \
                        unit is 'ticks'(one tick is 10^-12 second)")
    parser.add_argument("--l2_ret", action="store", type=str,
                        default="10Tticks",
                        help="Retention time setting of L2, \
                        unit is 'ticks'(one tick is 10^-12 second)")
    parser.add_argument("--l3_ret", action="store", type=str,
                        default="10Tticks",
                        help="Retention time setting of L3, \
                        unit is 'ticks'(one tick is 10^-12 second)")
    parser.add_argument("--mmul2_ret", action="store", type=str,
                        default="10Tticks",
                        help="Retention time setting of MMUL2, \
                        unit is 'ticks'(one tick is 10^-12 second)")
    parser.add_argument("--last_cache_level", type=int, default=3,
                        help = "number of cache levels")
    parser.add_argument("--checkpoint", action="store_true")
    parser.add_argument("--parsec_checkpoint", action="store_true")
    parser.add_argument("--restore", type=str, default=None)
    parser.add_argument("--maxinsts", type=int, default=1000000000,
                        help = "number of maximum simulation \
                        insts in any thread")
    parser.add_argument("--l1d_pfexp", type=str2bool, default=True,
                        help="Determine L1 data prefetcher to prefetch \
                        an expired block or not")
    parser.add_argument("--l1d_pfd", type=int, default=4,
                        help = "L1 data cache prefetch degree")
    parser.add_argument("--l1i_wrLat", type=int, default=1,
                        help = "L1 instruction cache write cycles")
    parser.add_argument("--l1d_wrLat", type=int, default=5,
                        help = "L1 data cache write cycles")
    parser.add_argument("--l1dw_wrLat", type=int, default=5,
                        help = "L1 data walker cache write cycles")
    parser.add_argument("--mmul2_wrLat", type=int, default=5,
                        help = "Shared walker cache write cycles")
    parser.add_argument("--l1d_hitLat", type=int, default=1,
                        help = "L1 data cache hit cycles")
    parser.add_argument("--l1dw_hitLat", type=int, default=1,
                        help = "L1 data walker cache hit cycles")
    parser.add_argument("--mmul2_hitLat", type=int, default=1,
                        help = "Shared walker cache hit cycles")
    parser.add_argument("--l1d_pfnst", type=str2bool, default=False,
                        help="Activate Nearside Prefetch Throttling in \
                        L1 data prefetcher")
    parser.add_argument("--mmu_cache", type=int, default=0,
                        help="0: use dedicated caches in MMU. 1: direct \
                        mmu requests to L1 I/Dcache. Other: direct mmu \
                        requests to next level caches/memory")


    args = parser.parse_args()

    root = Root(full_system=True)
    root.system = create(args)

    if args.restore is not None:
        m5.instantiate(args.restore)
    else:
        m5.instantiate()

    run(args,root)


if __name__ == "__m5_main__":
    main()
